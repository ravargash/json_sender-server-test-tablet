#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QTcpServer>
#include <QHostAddress>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void conexionEntrante();

private slots:
    void readyRead();
    void disconected();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_7_clicked();
    void on_pushButton_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_8_clicked();
    void on_pushButton_14_clicked();
    void on_pushButton_11_clicked();
    void on_pushButton_13_clicked();
    void on_pushButton_10_clicked();
    void on_pushButton_16_clicked();
    void on_pushButton_17_clicked();

    
    void on_pushButton_6_clicked();

    void on_pushButton_18_clicked();

    void on_pushButton_19_clicked();

    void on_pushButton_20_clicked();

    void on_pushButton_21_clicked();

private:
    Ui::MainWindow *ui;
    void fillLists();
    QTcpSocket* soquete;
    QTcpServer* server;
    QString deviceMAC;
    QString deviceId,userId,gradeId,studentId,accessId,apId;
    bool isEntry;


    QList <QString> studentsNameList;
    QList <QString> studentsLastNameList;
    QList <QString> studentsIdList;
    QList <bool> studentsMaleList;
    QList <QString> studentsStatusList;

    QList <QString> teacherNameList;
    QList <QString> teacherLastNameList;
    QList <QString> teacherIdList;

    QList <QString> inspectorNameList;
    QList <QString> inspectorLastNameList;
    QList <QString> instepctorIdList;

    QList <QString> classNameList;
    QList <QString> parNameList;
    QList <QString> classIdList;

    QList <QString> accessNameList;


    QList <QString> reasonInList;
    QList <QString> reasonInId;
    QList <QString> reasonOutList;
    QList <QString> reasonOutId;


};

#endif // MAINWINDOW_H
