#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    deviceId = -1;
    userId = -1;
    gradeId= -1;
    studentId = -1;
    accessId = -1;
    isEntry = false;
    ui->setupUi(this);
    server = new QTcpServer();
    server->listen(QHostAddress::Any,5740);
    connect(server,SIGNAL(newConnection()),this,SLOT(conexionEntrante()));

    fillLists();

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::conexionEntrante(){
    soquete = server->nextPendingConnection();
    connect(soquete,SIGNAL(readyRead()),this,SLOT(readyRead()));
    connect(soquete,SIGNAL(disconnected()),this,SLOT(disconected()));
    ui->textEdit->append("Conectado con el receptor");
}

void MainWindow::disconected(){
    ui->textEdit->append("Desconectado");
}


void MainWindow::readyRead(){
    QByteArray qba = soquete->readAll();
    QJsonObject jsonObjMain = QJsonDocument::fromJson(qba).object(), jsonObjData;

    QString jsonType = jsonObjMain["type"].toString();
    jsonObjData = jsonObjMain["data"].toObject();
    ui->textEdit->append("Llego paquete " + jsonType);

    if(jsonType == "insp_init"){
        deviceId = QString::number(rand());
        deviceMAC = jsonObjData["deviceMac"].toString();
        ui->textEdit->append(" /");
        ui->textEdit->append("| deviceMac: " + deviceMAC);
        ui->textEdit->append(" \\");
        if(ui->checkBox_5->isChecked())on_pushButton_2_clicked();
    }

    if(jsonType == "insp_roleSelect"){
        deviceId = jsonObjData["deviceId"].toString();
        ui->textEdit->append(" /");
        ui->textEdit->append("| deviceID: " + deviceId);
        ui->textEdit->append("| roleId: " + jsonObjData["roleId"].toString());
        ui->textEdit->append(" \\");
        if(ui->checkBox_5->isChecked())on_pushButton_16_clicked();
    }

    if(jsonType == "insp_selectUser"){
        deviceId = jsonObjData["deviceId"].toString();
        userId = jsonObjData["userId"].toString();
        ui->textEdit->append(" /");
        ui->textEdit->append("| deviceId: " + deviceId);
        ui->textEdit->append("| userId: " + userId);
        ui->textEdit->append(" \\");
        if(ui->checkBox_5->isChecked())on_pushButton_3_clicked();
    }

    if(jsonType == "insp_selectGrade"){
        QString isEntryString;
        deviceId = jsonObjData["deviceId"].toString();
        userId = jsonObjData["userId"].toString();
        gradeId = jsonObjData["gradeId"].toString();
        isEntry = jsonObjData["isEntry"].toBool();
        isEntryString = isEntry? "true": "false";
        ui->textEdit->append(" /");
        ui->textEdit->append("| deviceId: " + deviceId);
        ui->textEdit->append("| userId: " + userId);
        ui->textEdit->append("| gradeId: " + gradeId);
        ui->textEdit->append("| isEntry: " + isEntryString);
        ui->textEdit->append(" \\");
        if(ui->checkBox_5->isChecked())on_pushButton_4_clicked();
    }

    if(jsonType == "insp_selectStudent"){
        QString isEntryString;
        deviceId = jsonObjData["deviceId"].toString();
        userId = jsonObjData["userId"].toString();
        gradeId = jsonObjData["gradeId"].toString();
        studentId = jsonObjData["studentId"].toString();
        isEntry = jsonObjData["isEntry"].toBool();
        isEntryString = isEntry? "true": "false";
        ui->textEdit->append(" /");
        ui->textEdit->append("| deviceId: " + deviceId);
        ui->textEdit->append("| userId: " + userId);
        ui->textEdit->append("| gradeId: " + gradeId);
        ui->textEdit->append("| studentId: " + studentId);
        ui->textEdit->append("| isEntry: " + isEntryString);
        ui->textEdit->append(" \\");
        if(ui->checkBox_5->isChecked())on_pushButton_clicked();
    }

    if(jsonType == "insp_studentAccess"){
        QString isEntryString,reasonText,reasonId;
        deviceId = jsonObjData["deviceId"].toString();
        userId = jsonObjData["userId"].toString();
        gradeId = jsonObjData["gradeId"].toString();
        studentId = jsonObjData["studentId"].toString();
        accessId = jsonObjData["accessId"].toString();
        isEntry = jsonObjData["isEntry"].toBool();
        isEntryString = isEntry? "true": "false";
        reasonText = jsonObjData["reasonText"].toString();
        reasonId = jsonObjData["reasonId"].toString();
        ui->textEdit->append(" /");
        ui->textEdit->append("| deviceId: " + deviceId);
        ui->textEdit->append("| userId: " + userId);
        ui->textEdit->append("| gradeId: " + gradeId);
        ui->textEdit->append("| studentId: " + studentId);
        ui->textEdit->append("| isEntry: " + isEntryString);
        ui->textEdit->append("| reasonText: " + reasonText);
        ui->textEdit->append("| reasonId: " + reasonId);
        ui->textEdit->append(" \\");
        if(ui->checkBox_5->isChecked())on_pushButton_5_clicked();
    }

    if(jsonType == "sc_init"){
        deviceId = QString::number(rand());
        deviceMAC = jsonObjData["deviceMac"].toString();
        ui->textEdit->append(" /");
        ui->textEdit->append("| deviceMac: " + deviceMAC);
        ui->textEdit->append(" \\");
        if(ui->checkBox_5->isChecked())on_pushButton_14_clicked();
    }

    if(jsonType == "sc_roleSelect"){
        deviceId = jsonObjData["deviceId"].toString();
        ui->textEdit->append(" /");
        ui->textEdit->append("| deviceID: " + deviceId);
        ui->textEdit->append("| roleId: " +jsonObjData["roleId"].toString());
        ui->textEdit->append(" \\");
        if(ui->checkBox_5->isChecked())on_pushButton_17_clicked();
    }

    if(jsonType == "sc_selectUser"){
        deviceId = jsonObjData["deviceId"].toString();
        userId = jsonObjData["userId"].toString();
        ui->textEdit->append(" /");
        ui->textEdit->append("| deviceId: " + deviceId);
        ui->textEdit->append("| userId: " + userId);
        ui->textEdit->append(" \\");
        if(ui->checkBox_5->isChecked())on_pushButton_11_clicked();
    }

    if(jsonType == "sc_selectGrade"){
        deviceId = jsonObjData["deviceId"].toString();
        userId = jsonObjData["userId"].toString();
        gradeId = jsonObjData["gradeId"].toString();
        ui->textEdit->append(" /");
        ui->textEdit->append("| deviceId: " + deviceId);
        ui->textEdit->append("| userId: " + userId);
        ui->textEdit->append("| classId: " + gradeId);
        ui->textEdit->append(" \\");
        if(ui->checkBox_5->isChecked())on_pushButton_13_clicked();
    }

    if(jsonType == "sc_updateStudent"){
        QString isPresentString,reasonText,reasonId;
        deviceId = jsonObjData["deviceId"].toString();
        userId = jsonObjData["userId"].toString();
        gradeId = jsonObjData["gradeId"].toString();
        studentId = jsonObjData["studentId"].toString();
        isPresentString = jsonObjData["isPresent"].toBool()? "true":"false";
        reasonText = jsonObjData["reasonText"].toString();
        reasonId = jsonObjData["reasonId"].toString();
        ui->textEdit->append(" /");
        ui->textEdit->append("| deviceId: " + deviceId);
        ui->textEdit->append("| userId: " + userId);
        ui->textEdit->append("| gradeId: " + gradeId);
        ui->textEdit->append("| isPresent: " + isPresentString);
        ui->textEdit->append("| reasonText: " + reasonText);
        ui->textEdit->append("| reasonId: " + reasonId);
        ui->textEdit->append(" \\");
        if(ui->checkBox_5->isChecked())on_pushButton_10_clicked();
    }

    if(jsonType == "sc_confirmClass"){
        deviceId = jsonObjData["deviceId"].toString();
        userId = jsonObjData["userId"].toString();
        gradeId = jsonObjData["gradeId"].toString();

        ui->textEdit->append(" /");
        ui->textEdit->append("| deviceId: " + deviceId);
        ui->textEdit->append("| userId: " + userId);
        ui->textEdit->append("| gradeId: " + gradeId);
        ui->textEdit->append(" \\");
    }

    if(jsonType == "ap_init"){
        deviceId = QString::number(rand());
        deviceMAC = jsonObjData["deviceMac"].toString();
        ui->textEdit->append(" /");
        ui->textEdit->append("| deviceMac: " + deviceMAC);
        ui->textEdit->append(" \\");
        if(ui->checkBox_5->isChecked())on_pushButton_6_clicked();
    }

    if(jsonType == "ap_ci"){
        deviceId = jsonObjData["deviceId"].toString();
        QString chorizo = jsonObjData["ciData"].toString();
        apId = QString::number(rand());
        ui->textEdit->append(" /");
        ui->textEdit->append("| deviceId: " + deviceId);
        ui->textEdit->append("| ciData: " + chorizo);
        ui->textEdit->append(" \\");
        if(ui->checkBox_5->isChecked())on_pushButton_18_clicked();
    }

    if(jsonType == "ap_studentSelection"){
        deviceId = jsonObjData["deviceId"].toString();
        apId = jsonObjData["apId"].toString();
        ui->textEdit->append(" /");
        ui->textEdit->append("| deviceId: " + deviceId);
        ui->textEdit->append("| apId: " + apId);
        ui->textEdit->append("| studentsIdList: ");
        for(int i = 0; i < jsonObjData["studentsIdList"].toArray().size(); i++){
            ui->textEdit->append("|   -" + jsonObjData["studentsIdList"].toArray()[i].toString());
        }
        ui->textEdit->append(" \\");
        if(ui->checkBox_5->isChecked())on_pushButton_19_clicked();
    }



}

void MainWindow::on_pushButton_2_clicked(){ //insp_appInit

    QJsonObject jsonObjMain,jsonObjData,jsonObjPalette,jsonObjRGB;
    QJsonArray jsonArray,jsonArrayRoles,jsonArrayReasonsIn,jsonArrayReasonsOut;

    jsonObjData["deviceId"] = deviceId;

    for(int i = 0; i < 4; i++){
        QJsonObject jsonObjTmp;
        jsonObjTmp["id"] =  QString::number(rand());
        jsonObjTmp["name"] = "Rol " + QString::number(i);
        jsonArrayRoles.append(jsonObjTmp);
    }
    jsonObjData["roleList"] = jsonArrayRoles;

    for(int i = 0; i < 3; i++){
        QJsonObject jsonObjTmp;
        jsonObjTmp["id"] =  QString::number(rand());
        jsonObjTmp["text"] = "Razón entrada " + QString::number(i);
        jsonArrayReasonsIn.append(jsonObjTmp);
    }
    jsonObjData["reasonsInList"] = jsonArrayReasonsIn;

    for(int i = 0; i < 3; i++){
        QJsonObject jsonObjTmp;
        jsonObjTmp["id"] =  QString::number(rand());
        jsonObjTmp["text"] = "Razón salida" + QString::number(i);
        jsonArrayReasonsOut.append(jsonObjTmp);
    }
    jsonObjData["reasonsOutList"] = jsonArrayReasonsOut;

    jsonObjData["defaultRole"] = "Rol 2";

    for(int i = 0; i < ui->spinBox->value(); i++){
       QJsonObject jsonObjTmp;
       jsonObjTmp["id"] = instepctorIdList[i];
       jsonObjTmp["name"] = inspectorNameList[i];
       jsonObjTmp["last_name"] = inspectorLastNameList[i];
       jsonArray.append(jsonObjTmp);
    }
    jsonObjData["defaultUserList"] = jsonArray;

    jsonObjRGB["r"] = ui->baseR->value();
    jsonObjRGB["g"] = ui->baseG->value();
    jsonObjRGB["b"] = ui->baseB->value();
    jsonObjPalette["base"] = jsonObjRGB;

    jsonObjRGB["r"] = ui->secR->value();
    jsonObjRGB["g"] = ui->secG->value();
    jsonObjRGB["b"] = ui->secB->value();
    jsonObjPalette["secondary"] = jsonObjRGB;

    jsonObjRGB["r"] = ui->trimR->value();
    jsonObjRGB["g"] = ui->trimG->value();
    jsonObjRGB["b"] = ui->trimB->value();
    jsonObjPalette["trim"] = jsonObjRGB;


    jsonObjData["palette"] = jsonObjPalette;

    jsonObjMain["type"] = "insp_appInit";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}

void MainWindow::on_pushButton_16_clicked() //insp_userList
{
    QJsonObject jsonObjMain,jsonObjData;
    QJsonArray jsonArrayUserList;


    jsonObjData["deviceId"] = deviceId;

    for(int i = 0; i < ui->spinBox->value(); i++){
       QJsonObject jsonObjTmp;
       jsonObjTmp["id"] = teacherIdList[i];
       jsonObjTmp["name"] = teacherNameList[i];
       jsonObjTmp["last_name"] = teacherLastNameList[i];
       jsonArrayUserList.append(jsonObjTmp);
    }
    jsonObjData["userList"] = jsonArrayUserList;

    jsonObjMain["type"] = "insp_userList";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}

void MainWindow::on_pushButton_3_clicked() //insp_gradesList
{


    QJsonObject jsonObjMain,jsonObjData;
    QJsonArray jsonArrayGrades;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["userName"] = "nombre_usuario";
    jsonObjData["userLastName"] = "apellido_usuario";

    for(int i = 0; i < ui->spinBox_2->value(); i++){
        for(int j = 0; j < ui->spinBox_3->value(); j++){
            QJsonObject jsonObjTmp;
            jsonObjTmp["name"] = classNameList[i];
            jsonObjTmp["id"] = QString::number(rand());
            jsonObjTmp["par"] = parNameList[j];
            jsonArrayGrades.append(jsonObjTmp);
        }
    }
    jsonObjData["gradesList"] = jsonArrayGrades;

    jsonObjMain["type"] = "insp_gradesList";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}

void MainWindow::on_pushButton_4_clicked() //insp_studentsList
{

    QJsonObject jsonObjMain,jsonObjData;
    QJsonArray jsonArrayStudents;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["gradeId"] = gradeId;
    jsonObjData["gradeName"] = "curso_seleccionado";
    jsonObjData["parName"] = "paralelo_seleccionado";
    jsonObjData["isEntry"] = (ui->checkBox_2->isChecked())?  true: false;

    for(int i = 0; i < ui->spinBox_4->value(); i++){
       QJsonObject jsonObjTmp;
       jsonObjTmp["id"] = studentsIdList[i];
       jsonObjTmp["name"] = studentsNameList[i];
       jsonObjTmp["last_name"] = studentsLastNameList[i];
       jsonArrayStudents.append(jsonObjTmp);
    }
    jsonObjData["studentsList"] = jsonArrayStudents;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "insp_studentsList";

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}

void MainWindow::on_pushButton_clicked() //insp_accessesList
{

    QJsonObject jsonObjMain,jsonObjData;
    QJsonArray jsonArrayStudents,jsonArrayReasons;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["gradeId"] = gradeId;
    jsonObjData["studentId"] = studentId;
    jsonObjData["studentName"] = "nombre_alumno";
    jsonObjData["studentLastName"] = "apellido_alumno";
    jsonObjData["isEntry"] = isEntry;


    for(int i = 0; i < ui->spinBox_5->value(); i++){
       QJsonObject jsonObjTmp;
       jsonObjTmp["id"] = "ZXY"+ QString::number(rand());
       jsonObjTmp["code_device"] = accessNameList[i];
       jsonObjTmp["enable"] = true;
       jsonArrayStudents.append(jsonObjTmp);
    }
    jsonObjData["accessesList"] = jsonArrayStudents;

    jsonObjMain["type"] = "insp_accessesList";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}

void MainWindow::on_pushButton_5_clicked() //insp_confirmation ok
{

    QJsonObject jsonObjMain,jsonObjData;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["allowed"] = true;
    jsonObjData["name"] = "Nombre";
    jsonObjData["lastName"] = "Apellido";
    jsonObjData["gradeName"] = "Septimo";
    jsonObjData["parName"] = "A";
    jsonObjData["isEntry"] = isEntry;
    jsonObjData["reason"] = "Apoderado sin CI";

    jsonObjMain["type"] = "insp_confirmation";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}

void MainWindow::on_pushButton_8_clicked() //insp_confirmation fail
{
    QJsonObject jsonObjMain,jsonObjData;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["allowed"] = false;
    jsonObjData["name"] = "Nombre";
    jsonObjData["lastName"] = "Apellido";
    jsonObjData["gradeName"] = "Septimo";
    jsonObjData["parName"] = "A";
    jsonObjData["isEntry"] = (ui->checkBox_2->isChecked())?  true: false;
    jsonObjData["reason"] = "Apoderado sin CI";

    jsonObjMain["type"] = "insp_confirmation";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}

void MainWindow::on_pushButton_14_clicked() //sc_appInit
{
    QJsonObject jsonObjMain,jsonObjData,jsonObjPalette,jsonObjRGB;
    QJsonArray jsonArray,jsonArrayReasonsPresent,jsonArrayReasonsAbscent,jsonArrayRoles;

    jsonObjData["deviceId"] = deviceId;

    for(int i = 0; i < 4; i++){
        QJsonObject jsonObjTmp;
        jsonObjTmp["id"] =  QString::number(rand());
        jsonObjTmp["name"] = "Rol " + QString::number(i);
        jsonArrayRoles.append(jsonObjTmp);
    }
    jsonObjData["roleList"] = jsonArrayRoles;


    jsonObjData["defaultRole"] = "Rol 1";

    for(int i = 0; i < ui->spinBox->value(); i++){
       QJsonObject jsonObjTmp;
       jsonObjTmp["id"] = instepctorIdList[i];
       jsonObjTmp["name"] = inspectorNameList[i];
       jsonObjTmp["last_name"] = inspectorLastNameList[i];
       jsonArray.append(jsonObjTmp);
    }
    jsonObjData["defaultUserList"] = jsonArray;

    for(int i = 0; i < 3; i++){
        QJsonObject jsonObjTmp;
        jsonObjTmp["id"] =  QString::number(rand());
        jsonObjTmp["text"] = "Razón ausente " + QString::number(i);
        jsonArrayReasonsAbscent.append(jsonObjTmp);
    }

    jsonObjData["reasonsAbsentList"] = jsonArrayReasonsAbscent;

    for(int i = 0; i < 3; i++){
        QJsonObject jsonObjTmp;
        jsonObjTmp["id"] =  QString::number(rand());
        jsonObjTmp["text"] = "Razón presente " + QString::number(i);
        jsonArrayReasonsPresent.append(jsonObjTmp);
    }
    jsonObjData["reasonsPresentList"] = jsonArrayReasonsPresent;

            jsonObjRGB["r"] = ui->baseR->value();
            jsonObjRGB["g"] = ui->baseG->value();
            jsonObjRGB["b"] = ui->baseB->value();
        jsonObjPalette["base"] = jsonObjRGB;

            jsonObjRGB["r"] = ui->secR->value();
            jsonObjRGB["g"] = ui->secG->value();
            jsonObjRGB["b"] = ui->secB->value();
        jsonObjPalette["secondary"] = jsonObjRGB;

            jsonObjRGB["r"] = ui->trimR->value();
            jsonObjRGB["g"] = ui->trimG->value();
            jsonObjRGB["b"] = ui->trimB->value();
        jsonObjPalette["trim"] = jsonObjRGB;


    jsonObjData["palette"] = jsonObjPalette;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "sc_appInit";

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}

void MainWindow::on_pushButton_17_clicked() //sc_userList
{
    QJsonObject jsonObjMain,jsonObjData;
    QJsonArray jsonArrayUserList;

    jsonObjData["deviceId"] = deviceId;

    for(int i = 0; i < ui->spinBox->value(); i++){
       QJsonObject jsonObjTmp;
       jsonObjTmp["id"] = teacherIdList[i];
       jsonObjTmp["name"] = teacherNameList[i];
       jsonObjTmp["last_name"] = teacherLastNameList[i];
       jsonArrayUserList.append(jsonObjTmp);
    }
    jsonObjData["userList"] = jsonArrayUserList;


    jsonObjMain["type"] = "sc_userList";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}

void MainWindow::on_pushButton_11_clicked() //sc_gradesList
{
    QJsonObject jsonObjMain,jsonObjData;
    QJsonArray jsonArrayGrades;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["userName"] = "nombre_profesor";
    jsonObjData["userLastName"] = "apellido_profesor";

    for(int i = 0; i < ui->spinBox_2->value(); i++){
        for(int j = 0; j < ui->spinBox_3->value(); j++){
            QJsonObject jsonObjTmp;
            jsonObjTmp["name"] = classNameList[i];
            jsonObjTmp["id"] = QString::number(rand());
            jsonObjTmp["par"] = parNameList[j];
            jsonArrayGrades.append(jsonObjTmp);
        }
    }
    jsonObjData["gradesList"] = jsonArrayGrades;


    jsonObjMain["type"] = "sc_gradesList";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}

void MainWindow::on_pushButton_13_clicked() //sc_studentsLists
{
    QJsonObject jsonObjMain,jsonObjData;
    QJsonArray jsonArrayStudents;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["gradeId"] = gradeId;
    jsonObjData["gradeName"] = "curso_seleccionado";
    jsonObjData["parName"] = "paralelo_seleccionadol";

    for(int i = 0; i < ui->spinBox_4->value(); i++){
       QJsonObject jsonObjTmp;
       jsonObjTmp["id"] = studentsIdList[i];
       jsonObjTmp["name"] = studentsNameList[i];
       jsonObjTmp["last_name"] = studentsLastNameList[i];
       jsonObjTmp["isMale"] = (rand()%2)?  true: false;
       jsonObjTmp["isPresent"] = ui->checkBox->isChecked()?  true: false;
       jsonObjTmp["isCalled"] = false;
       jsonArrayStudents.append(jsonObjTmp);
    }
    jsonObjData["studentsList"] = jsonArrayStudents;

    jsonObjMain["type"] = "sc_studentsList";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}

void MainWindow::on_pushButton_10_clicked() //sc_updateStudent
{
    QJsonObject jsonObjMain,jsonObjData,jsonObjTmp;
    QJsonArray jsonArrayStudents;

    jsonObjData["deviceId"] = deviceId;

        jsonObjTmp["id"] = studentsIdList[ui->spinBox_10->value()];
        jsonObjTmp["isPresent"] = ui->checkBox_3->isChecked()? true: false;
        jsonObjTmp["isCalled"] = ui->checkBox_4->isChecked()? true: false;
        jsonArrayStudents.append(jsonObjTmp);
    jsonObjData["studentsList"] = jsonArrayStudents;

    jsonObjMain["type"] = "sc_updateStudent";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}

void MainWindow::on_pushButton_21_clicked()
{
    QJsonObject jsonObjMain,jsonObjData;

    QString text = ui->lineEdit->text();
    QString level = QString::number(ui->spinBox_11->value());

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["level"] = level;
    jsonObjData["text"] = text;
    jsonObjData["title"] = "Titulo";


    jsonObjMain["type"] = "sc_info";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());

}

void MainWindow::on_pushButton_6_clicked() //ap_appInit
{
    QJsonObject jsonObjMain,jsonObjData,jsonObjPalette,jsonObjRGB;;

    jsonObjData["deviceId"] = deviceId;

            jsonObjRGB["r"] = ui->baseR->value();
            jsonObjRGB["g"] = ui->baseG->value();
            jsonObjRGB["b"] = ui->baseB->value();
        jsonObjPalette["base"] = jsonObjRGB;

            jsonObjRGB["r"] = ui->secR->value();
            jsonObjRGB["g"] = ui->secG->value();
            jsonObjRGB["b"] = ui->secB->value();
        jsonObjPalette["secondary"] = jsonObjRGB;

            jsonObjRGB["r"] = ui->trimR->value();
            jsonObjRGB["g"] = ui->trimG->value();
            jsonObjRGB["b"] = ui->trimB->value();
        jsonObjPalette["trim"] = jsonObjRGB;


    jsonObjData["palette"] = jsonObjPalette;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "ap_appInit";

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}

void MainWindow::on_pushButton_18_clicked() //ap_studentsList
{
    QJsonObject jsonObjMain,jsonObjData;
    QJsonArray jsonArrayStudents;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["apId"] = apId;
    jsonObjData["name"] = "nombre_apoderado";
    jsonObjData["lastName"] = "apellido_apoderado";

    for(int i = 0; i < ui->spinBox_4->value(); i++){
       QJsonObject jsonObjTmp;
       jsonObjTmp["id"] = studentsIdList[i];
       jsonObjTmp["isMale"] = (rand()%2)?  true: false;
       jsonObjTmp["name"] = studentsNameList[i];
       jsonObjTmp["last_name"] = studentsLastNameList[i];
       jsonObjTmp["isActual"] = (rand()%2)?  true: false;
       jsonArrayStudents.append(jsonObjTmp);
    }
    jsonObjData["studentsList"] = jsonArrayStudents;

    jsonObjMain["type"] = "ap_studentsList";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}

void MainWindow::on_pushButton_19_clicked()// ap_confirmation ok
{
    QJsonObject jsonObjMain,jsonObjData;
    QJsonArray jsonArrayStudents;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["apId"] = apId;
    jsonObjData["isAllowed"] = true;
    jsonObjData["name"] = "nombre_apoderado";
    jsonObjData["lastName"] = "apellido_apoderado";

    for(int i = 0; i < ui->spinBox_4->value(); i++){
       QJsonObject jsonObjTmp;
       jsonObjTmp["name"] = studentsNameList[i];
       jsonObjTmp["lastName"] = studentsLastNameList[i];
       jsonArrayStudents.append(jsonObjTmp);
    }
    jsonObjData["studentsList"] = jsonArrayStudents;

    jsonObjMain["type"] = "ap_confirmation";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}

void MainWindow::on_pushButton_20_clicked()// ap_confirmation fail
{
    QJsonObject jsonObjMain,jsonObjData;
    QJsonArray jsonArrayStudents;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["apId"] = apId;
    jsonObjData["isAllowed"] = false;
    jsonObjData["name"] = "nombre_apoderado";
    jsonObjData["lastName"] = "apellido_apoderado";

    for(int i = 0; i < ui->spinBox_4->value(); i++){
       QJsonObject jsonObjTmp;
       jsonObjTmp["name"] = studentsNameList[i];
       jsonObjTmp["lastName"] = studentsLastNameList[i];
       jsonArrayStudents.append(jsonObjTmp);
    }
    jsonObjData["studentsList"] = jsonArrayStudents;

    jsonObjMain["type"] = "ap_confirmation";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    soquete->write(jsonDoc.toJson());
}



void MainWindow::on_pushButton_7_clicked()
{
    ui->textEdit->clear();
}

void MainWindow::fillLists(){
    for(int i = 0; i < 45; i++){studentsIdList.append(QString::number(rand()));
        studentsStatusList.append("present");
        studentsMaleList.append((bool)(rand()%2));
    }

    studentsNameList.append("Guillermina");
    studentsNameList.append("Eduardo");
    studentsNameList.append("Ignacia");
    studentsNameList.append("Josefina");
    studentsNameList.append("Agustín");
    studentsNameList.append("Antonia");
    studentsNameList.append("Gabriel");
    studentsNameList.append("Matías");
    studentsNameList.append("Sofía");
    studentsNameList.append("Catalina");
    studentsNameList.append("Emilia");
    studentsNameList.append("Diego");
    studentsNameList.append("William");
    studentsNameList.append("Juan Pablo");
    studentsNameList.append("Maximiliano");
    studentsNameList.append("Francisca");
    studentsNameList.append("Vicente");
    studentsNameList.append("Maximo");
    studentsNameList.append("Maximiliano");
    studentsNameList.append("Francisca");
    studentsNameList.append("Juan Carlos");
    studentsNameList.append("Sebastián");
    studentsNameList.append("Jorge");
    studentsNameList.append("Emilia");
    studentsNameList.append("María Teresa");
    studentsNameList.append("Diego Antonio");
    studentsNameList.append("Violeta");
    studentsNameList.append("Cosette");
    studentsNameList.append("Diego");
    studentsNameList.append("María Catalina");
    studentsNameList.append("Agustina");
    studentsNameList.append("Iñaki");
    studentsNameList.append("Diego");
    studentsNameList.append("Pedro Pablov");
    studentsNameList.append("Alen");
    studentsNameList.append("Rafaella");
    studentsNameList.append("Ema");
    studentsNameList.append("Joaquín");
    studentsNameList.append("Catalina");
    studentsNameList.append("Sara");
    studentsNameList.append("Dino");
    studentsNameList.append("Ignacio");
    studentsNameList.append("Matías");
    studentsNameList.append("Raimundo");
    studentsNameList.append("Trinidad");
    studentsNameList.append("Sofía");

    studentsLastNameList.append("Ado");
    studentsLastNameList.append("Aguirre");
    studentsLastNameList.append("Arancibia");
    studentsLastNameList.append("Aravena");
    studentsLastNameList.append("Brinck");
    studentsLastNameList.append("Castro");
    studentsLastNameList.append("Castro");
    studentsLastNameList.append("Cuadros");
    studentsLastNameList.append("Gardella");
    studentsLastNameList.append("González");
    studentsLastNameList.append("Guevara");
    studentsLastNameList.append("Hermosilla");
    studentsLastNameList.append("Hole");
    studentsLastNameList.append("Horta");
    studentsLastNameList.append("Madrid");
    studentsLastNameList.append("Martínez");
    studentsLastNameList.append("Mella");
    studentsLastNameList.append("Meza");
    studentsLastNameList.append("Morales");
    studentsLastNameList.append("Muñoz");
    studentsLastNameList.append("Ovalle");
    studentsLastNameList.append("Pérez");
    studentsLastNameList.append("Pizarro");
    studentsLastNameList.append("Rojas");
    studentsLastNameList.append("Rubio");
    studentsLastNameList.append("Sanhueza");
    studentsLastNameList.append("Torres");
    studentsLastNameList.append("Tupper");
    studentsLastNameList.append("Vivanco");
    studentsLastNameList.append("Abufom");
    studentsLastNameList.append("Agnic");
    studentsLastNameList.append("Amenábar");
    studentsLastNameList.append("Barrios");
    studentsLastNameList.append("Bozzo");
    studentsLastNameList.append("Browne");
    studentsLastNameList.append("Cammas");
    studentsLastNameList.append("Canales");
    studentsLastNameList.append("Cristi");
    studentsLastNameList.append("Fresno");
    studentsLastNameList.append("Gatica");
    studentsLastNameList.append("Girardi");
    studentsLastNameList.append("Larenas");
    studentsLastNameList.append("León");
    studentsLastNameList.append("Manríquez");
    studentsLastNameList.append("Mina");
    studentsLastNameList.append("Panchana");



    teacherNameList.append("Santiago José");
    teacherNameList.append("Pascual");
    teacherNameList.append("Rafael Vicente");
    teacherNameList.append("Nicolás Francisco");
    teacherNameList.append("Elena Ignacia");
    teacherNameList.append("Mariana Paz");
    teacherNameList.append("Agustina");
    teacherNameList.append("Iñaki");
    teacherNameList.append("Diego");
    teacherNameList.append("Pedro Pablov");

    teacherLastNameList.append("Villavicencio");
    teacherLastNameList.append("Yates");
    teacherLastNameList.append("Arellano");
    teacherLastNameList.append("Benítez");
    teacherLastNameList.append("Campos");
    teacherLastNameList.append("Carpenter");
    teacherLastNameList.append("León");
    teacherLastNameList.append("Manríquez");
    teacherLastNameList.append("Mina");
    teacherLastNameList.append("Panchana");


    for(int i = 0; i < 10; i++) teacherIdList.append(QString::number(rand()));

    inspectorNameList.append("Santiago José");
    inspectorNameList.append("Pascual");
    inspectorNameList.append("Rafael Vicente");
    inspectorNameList.append("Nicolás Francisco");
    inspectorNameList.append("Elena Ignacia");
    inspectorNameList.append("Mariana Paz");
    inspectorNameList.append("Agustina");
    inspectorNameList.append("Iñaki");
    inspectorNameList.append("Diego");
    inspectorNameList.append("Pedro Pablov");

    inspectorLastNameList.append("Villavicencio");
    inspectorLastNameList.append("Yates");
    inspectorLastNameList.append("Arellano");
    inspectorLastNameList.append("Benítez");
    inspectorLastNameList.append("Campos");
    inspectorLastNameList.append("Carpenter");
    inspectorLastNameList.append("León");
    inspectorLastNameList.append("Manríquez");
    inspectorLastNameList.append("Mina");
    inspectorLastNameList.append("Panchana");


    for(int i = 0; i < 10; i++) instepctorIdList.append(QString::number(rand()));

    classNameList.append("Primero Básico");
    classNameList.append("Segundo Básico");
    classNameList.append("Tercero Básico");
    classNameList.append("Cuarto Básico");
    classNameList.append("Qunto Básico");
    classNameList.append("Sexto Básico");
    classNameList.append("Séptimo Básico");
    classNameList.append("Octavo Básico");
    classNameList.append("Primero Medio");
    classNameList.append("Segundo Medio");
    classNameList.append("Tercero Medio");
    classNameList.append("Cuarto Medio");

    parNameList.append("A");
    parNameList.append("B");
    parNameList.append("C");
    parNameList.append("D");
    parNameList.append("E");
    parNameList.append("F");
    parNameList.append("G");
    parNameList.append("H");

    accessNameList.append("Discapacitados Básica");
    accessNameList.append("Torniquete Básica 1");
    accessNameList.append("Torniquete Básica 2");
    accessNameList.append("Torniquete Básica 3");
    accessNameList.append("Torniquete Básica 4");
    accessNameList.append("Torniquete Básica 5");
    accessNameList.append("Discapacitados Media");
    accessNameList.append("Torniquete Media 1");
    accessNameList.append("Torniquete Media 2");
    accessNameList.append("Torniquete Media 3");
    accessNameList.append("Torniquete Media 4");
    accessNameList.append("Torniquete Media 5");
    accessNameList.append("Puerta Bacán 1");
    accessNameList.append("Puerta Bacán 2");
    accessNameList.append("Puerta Bacán 3");
    accessNameList.append("Puerta Bacán 4");
}


